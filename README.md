# README #

![Image of task](task.png)

[URL Manual](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04)


## Problemas

* Fichero de configuración de mysql, en la nueva version, fue movido a la ruta /etc/mysql/mysql.conf.d/mysqld.cnf

* Al usar el composer installer, cuando introduces los datos de la base mysql si la IP la escribes asi '127.0.0.1' te rechaza la conexión. Hay que sustituir esos valores por 'localhost'.